
import json
import datetime
import decimal
#Get number of days for the cuurent mov
print('Enter the days you want to consider for moving average')
movRecent5 = input()
print('Enter the days you want to consider for moving average this should be greater than the previous one')
movRecent8 = input() 
print('Enter the date you want to calculate moving average for in format: YYYY-MM-DD example(2021-03-19)')
dateToCalculate = input() 

with open('newInput_josn.json', 'r') as j:
    json_data = json.load(j)
    timeStampList = json_data['chart']['result'][0]['timestamp']
    maxPrice = json_data['chart']['result'][0]['indicators']['quote'][0]['high']
    minPrice = json_data['chart']['result'][0]['indicators']['quote'][0]['low']
    i = 0
    compileddata = []
    while i < len(timeStampList):
        data = {}
        if maxPrice[i] is not None:
            data['date'] = datetime.datetime.fromtimestamp(int(timeStampList[i])).strftime('%Y-%m-%d %H:%M:%S')
            data['max'] = maxPrice[i]
            data['min'] = minPrice[i]
            data['avg'] = (float(str(data['max'])) + float(str(data['min'])))/ int("2")
            compileddata.append(data)
        i = i+1
    #with open('output_data.json', 'w') as file:
        #json.dump(compileddata, file)        
    #print(compileddata)
    #data is created in a perticular format now we need to calculate moving average for days passed by the user
    startOfMovingAverage5 = -1
    endOfMovingAverage5 = -1
    startOfMovingAverage8 = -1
    endOfMovingAverage8 = -1
    endIndex = -1
    #figureout start and end of the moving average
    i = len(compileddata)-1
    count5 = int(movRecent5)
    count8 = int(movRecent8)
    startingPointFound = 0
    while i>=0 :
        if not startingPointFound :
            if compileddata[i]['date'].startswith(dateToCalculate):
                endIndex = i
                while 1:
                    if not compileddata[i]['date'].startswith(dateToCalculate):
                        startOfMovingAverage5 = i+1
                        startOfMovingAverage8 = i+1
                        i = i+1
                        startingPointFound = 1
                        break
                    else:
                        i = i-1
        else :
            #now we know the starting point and we are interested in endpoints
            #print(1)
            #print(compileddata[i]['date'][0 : 10])
            if compileddata[i]['date'][0 : 10] != compileddata[i+1]['date'][0 : 10]:
                count5 = count5 - 1
                count8 = count8 - 1
                if count5 == -1 and endOfMovingAverage5 == -1:
                    endOfMovingAverage5 = i+2
                if count8 == -1 :
                    endOfMovingAverage8 = i+2

        if startOfMovingAverage5 != -1 and startOfMovingAverage8 != -1 and endOfMovingAverage5 != -1 and endOfMovingAverage8 != -1:
            break;
        i = i-1

    #start of moving averages
    movTotal5 = float('0')
    movTotal8 = float('0')
    movDiv5 = 0
    movDiv8 = 0
    i = startOfMovingAverage5
    while i >= endOfMovingAverage5:
        movTotal5 = movTotal5 + float(compileddata[i]['avg'])
        movDiv5 = movDiv5+1
        i = i-1

    i = startOfMovingAverage8
    while i >= endOfMovingAverage8:
        movTotal8 = movTotal8 + float(compileddata[i]['avg'])
        movDiv8 = movDiv8+1
        i = i-1        
    mov5 = movTotal5 / movDiv5
    mov8 = movTotal8 / movDiv8
    Up8 = 0
    if mov8 >= mov5:
        Up8 = 1    
    #I have a moving average start point now we need to calculate how this goes
    i = startOfMovingAverage5+1
    while i<= endIndex:
        movTotal5 = movTotal5 - float(compileddata[endOfMovingAverage5]['avg']) + float(compileddata[i]['avg'])
        mov5 = movTotal5 / movDiv5
        endOfMovingAverage5 = endOfMovingAverage5 + 1
        movTotal8 = movTotal8 - float(compileddata[endOfMovingAverage8]['avg']) + float(compileddata[i]['avg'])
        mov8 = movTotal8 / movDiv8
        endOfMovingAverage8 = endOfMovingAverage8 + 1
        if Up8 and mov5>=mov8:
            print("put:  " +  str(compileddata[i]['date']) + ": " + str(compileddata[i]['avg']))
            Up8 = 0
        elif Up8 == 0 and mov8>=mov5:
            print("call: " + str(compileddata[i]['date']) + ": "+ str(compileddata[i]['avg']))
            Up8 = 1
        i = i+1
        print(str(mov5)+"   :    " + str(mov8))
        
